`docker-compose up`

`docker exec -it cassandra-rz1-node1 nodetool status`
`docker exec -it cassandra-rz1-node1 nodetool ring`
`docker exec -it cassandra-rz1-node1 cqlsh`

```
CREATE KEYSPACE repl_keyspace WITH REPLICATION = {'class': 'NetworkTopologyStrategy', 'dc1': 2};
-- CREATE KEYSPACE repl_keyspace WITH REPLICATION = {'class': 'NetworkTopologyStrategy', 'dc1': 2, 'dc2': 2};
use repl_keyspace;

create table headset (
    hersteller text,
    id text,
    bezeichnung text,
    kabellos boolean,
    PRIMARY KEY ((hersteller), id)
);

INSERT INTO headset (hersteller, id, bezeichnung, kabellos) VALUES ('Teufel', '93070e91-0e83-4011-98b5-85df58cebd0e', 'CAGE', false);
INSERT INTO headset (hersteller, id, bezeichnung, kabellos) VALUES ('Lioncast', '061e0e81-d861-4f3c-8b91-e638eae0e869', 'LX55 PRO', false);
INSERT INTO headset (hersteller, id, bezeichnung, kabellos) VALUES ('HyperX', '4cfdbbf8-8cb4-4e87-b829-1670504ede7b', 'Cloud II', false);
INSERT INTO headset (hersteller, id, bezeichnung, kabellos) VALUES ('HyperX', '4aa8be24-8d6f-4265-91c7-aef7d6a0d173', 'Cloud II Wireless', true);
INSERT INTO headset (hersteller, id, bezeichnung, kabellos) VALUES ('Corsair', '135e9cee-256b-43b7-a530-9a18a4c585af', 'Virtuoso RGB', true);


select token(hersteller), hersteller from headset;

TRACING ON;

CONSISTENCY ONE;
SELECT * FROM headset WHERE hersteller = 'HyperX';

CONSISTENCY QUORUM;
SELECT * FROM headset WHERE hersteller = 'HyperX';

TRACING OFF;

```

`docker-compose down`
